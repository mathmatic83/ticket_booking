module.exports = {
    root: true,
    env: {
      node: true
    },
    extends: [
      "plugin:vue/vue3-recommended",
      "eslint:recommended",
      "@vue/typescript/recommended",
      "@vue/prettier",
      "@vue/prettier/@typescript-eslint"
    ],
    parserOptions: {
      ecmaVersion: 2020
    },
    rules: {
      "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
      "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
      'global-require': 'off',
      'max-len': 'off',
      'prefer-destructuring': 'off',
      'no-control-regex': 'off',
      'arrow-parens': 'off',
      'vue/require-default-prop': 'off',
      'vue/no-v-html': 'off',
      'vue/html-self-closing': 'off',
      'vue/name-property-casing': 'off',
      'vue/max-attributes-per-line': [
        'warn',
        {
          'singleline': 3,
          'multiline': {
            'max': 1
          }
        }
      ]
    }
  };
  