import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/showing",
    name: "showing",
    component: () => import("../views/showing.vue")
  },
  {
    path: "/film",
    name: "film",
    component: () => import("../views/film.vue")
  }
  // {
  //   path: "/coming",
  //   name: "coming"
  // }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
