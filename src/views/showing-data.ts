const showingData = [
  {
    movieTw: "永恆族",
    movieEn: "ETERNALS",
    type: "pre-purchase",
    status: "預售中",
    level: "parental",
    commingDate: "2021-11-03",
    imgSrc: require("../assets/img/film_20211012008.jpeg")
  },
  {
    movieTw: "猛毒2: 血蜘蛛",
    movieEn: "VENOM : LET THERE BE CARNAGE",
    type: "champion",
    status: "票房冠軍",
    level: "parental-guidance",
    commingDate: "2021-10-13",
    imgSrc: require("../assets/img/film_20210917004.jpeg")
  },
  {
    movieTw: "007生死交戰",
    movieEn: "NO TIME TO DIE",
    type: "champion",
    status: "票房冠軍",
    level: "parental-guidance",
    commingDate: "2021-09-30",
    imgSrc: require("../assets/img/film_20210903008.jpeg")
  },
  {
    movieTw: "沙丘",
    movieEn: "DUNE",
    type: "champion",
    status: "票房冠軍",
    level: "parental",
    commingDate: "2021-09-16",
    imgSrc: require("../assets/img/film_20210817016.jpeg")
  },
  {
    movieTw: "月光光新慌慌: 萬聖殺",
    movieEn: "HALLOWEEN KILLS",
    type: "top-five",
    status: "票房前五",
    level: "restricted",
    commingDate: "2021-10-22",
    imgSrc: require("../assets/img/film_20211008002.jpeg")
  }
];

export default showingData;
