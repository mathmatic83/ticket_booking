const seatNumbers = [
  "",
  "",
  "1",
  "2",
  "",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "17",
  "",
  "18",
  "19"
];
const seatTable = [
  {
    index: "A",
    seatNumbers: seatNumbers
  },
  {
    index: "B",
    seatNumbers: seatNumbers
  },
  {
    index: "C",
    seatNumbers: seatNumbers
  },
  {
    index: "D",
    seatNumbers: seatNumbers
  },
  {
    index: "E",
    seatNumbers: seatNumbers
  },
  {
    index: "F",
    seatNumbers: seatNumbers
  },
  {
    index: "G",
    seatNumbers: seatNumbers
  },
  {
    index: "H",
    seatNumbers: seatNumbers
  }
];

export default seatTable;
